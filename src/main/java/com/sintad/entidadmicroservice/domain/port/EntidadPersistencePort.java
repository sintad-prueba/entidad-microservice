package com.sintad.entidadmicroservice.domain.port;

import com.sintad.entidadmicroservice.domain.model.Entidad;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
public interface EntidadPersistencePort {
    Mono<Entidad> save (Entidad model);
    Mono<Entidad> update (Entidad model);
    Mono<Void> deleteById (Long id);
    Flux<Entidad> findAll();
}
