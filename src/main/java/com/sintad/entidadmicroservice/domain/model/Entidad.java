package com.sintad.entidadmicroservice.domain.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author roberth
 */
@Getter
@Setter
@Builder
public class Entidad {
    private Long idEntidad;
    private Long idTipoDocumento;
    private String nroDocumento;
    private String razonSocial;
    private String nombreComercial;
    private Long idTipoContribuyente;
    private String direccion;
    private String telefono;
    private boolean estado;
}
