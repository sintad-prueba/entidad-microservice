package com.sintad.entidadmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntidadMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntidadMicroserviceApplication.class, args);
	}

}
