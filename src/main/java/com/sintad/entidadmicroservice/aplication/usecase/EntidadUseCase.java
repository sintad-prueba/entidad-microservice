/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sintad.entidadmicroservice.aplication.usecase;

import com.sintad.entidadmicroservice.domain.model.Entidad;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
public interface EntidadUseCase {
    Flux<Entidad> findAll();
    Mono<Entidad> save(Entidad model);
    Mono<Entidad> update(Entidad model);
    Mono<Void> deleteById(Long id);
}
