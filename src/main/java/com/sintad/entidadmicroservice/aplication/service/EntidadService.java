package com.sintad.entidadmicroservice.aplication.service;

import com.sintad.entidadmicroservice.aplication.usecase.EntidadUseCase;
import com.sintad.entidadmicroservice.domain.model.Entidad;
import com.sintad.entidadmicroservice.domain.port.EntidadPersistencePort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Service
@AllArgsConstructor
public class EntidadService implements EntidadUseCase{
    
    EntidadPersistencePort persistence;

    @Override
    public Flux<Entidad> findAll() {
        return persistence.findAll();
    }

    @Override
    public Mono<Entidad> save(Entidad model) {
        return persistence.save(model);
    }

    @Override
    public Mono<Entidad> update(Entidad model) {
        return persistence.update(model);
    }

    @Override
    public Mono<Void> deleteById(Long id) {
        return persistence.deleteById(id);
    }
    
}
