/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sintad.entidadmicroservice.infraestructure.rest.router;

import com.sintad.entidadmicroservice.infraestructure.rest.handler.EntidadHandler;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 *
 * @author roberth
 */
@Configuration
public class EntidadRouter {
    private static String path = "/api/entidad";

    @Bean
    public WebProperties.Resources resources() {
        return new WebProperties.Resources();
    }

    @Bean
    RouterFunction<ServerResponse> routerEntidad(EntidadHandler handler) {
        return RouterFunctions.route()
                .GET(path, handler::findAll)
                .POST(path, handler::save)
                .PUT(path, handler::update)
                .DELETE(path + "/{id}", handler::deleteById)
                .build();
    }
}
