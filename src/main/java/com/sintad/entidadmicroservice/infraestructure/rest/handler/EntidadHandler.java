package com.sintad.entidadmicroservice.infraestructure.rest.handler;

import com.sintad.entidadmicroservice.aplication.service.EntidadService;
import com.sintad.entidadmicroservice.domain.model.Entidad;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
@AllArgsConstructor
public class EntidadHandler {
    private final MediaType typeJson = MediaType.APPLICATION_JSON;
     
     EntidadService service;
     
     public Mono<ServerResponse> save(ServerRequest request){
        Mono<Entidad> tipoDocumento = request.bodyToMono(Entidad.class);
        
        return tipoDocumento.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(service.save(model), Entidad.class)
        );
    }
    
    public Mono<ServerResponse> update(ServerRequest request){
        Mono<Entidad> tipoDocumento = request.bodyToMono(Entidad.class);
        
        return tipoDocumento.flatMap(model -> ServerResponse.ok()
                .contentType(typeJson)
                .body(service.update(model), Entidad.class)
        );
    }
    
    public Mono<ServerResponse> deleteById(ServerRequest request){
        Long id = Long.valueOf(request.pathVariable("id"));
        
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.deleteById(id), Entidad.class);
    }
    
    public Mono<ServerResponse> findAll(ServerRequest request){
        return ServerResponse.ok()
                .contentType(typeJson)
                .body(service.findAll(), Entidad.class);
    }
}
