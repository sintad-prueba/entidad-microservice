/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sintad.entidadmicroservice.infraestructure.adapter.repository;

import com.sintad.entidadmicroservice.infraestructure.adapter.entity.EntidadEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberth
 */
@Repository
public interface EntidadRepository extends ReactiveCrudRepository<EntidadEntity, Long>{
    
}
