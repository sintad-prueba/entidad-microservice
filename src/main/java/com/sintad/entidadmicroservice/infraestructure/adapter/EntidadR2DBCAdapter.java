/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sintad.entidadmicroservice.infraestructure.adapter;

import com.sintad.entidadmicroservice.domain.model.Entidad;
import com.sintad.entidadmicroservice.domain.port.EntidadPersistencePort;
import com.sintad.entidadmicroservice.infraestructure.adapter.entity.EntidadEntity;
import com.sintad.entidadmicroservice.infraestructure.adapter.repository.EntidadRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author roberth
 */
@Service
@AllArgsConstructor
public class EntidadR2DBCAdapter implements EntidadPersistencePort {

    EntidadRepository repository;

    @Override
    public Mono<Entidad> save(Entidad model) {
        return repository.save(EntidadEntity.builder().build().fromDomain(model))
                .map(entity -> entity.toDomain(entity));
    }

    @Override
    public Mono<Entidad> update(Entidad model) {
        return repository.save(EntidadEntity.builder().build().fromDomain(model))
                .map(entity -> entity.toDomain(entity));
    }

    @Override
    public Mono<Void> deleteById(Long id) {
        return repository.deleteById(id);
    }

    @Override
    public Flux<Entidad> findAll() {
        return repository.findAll()
                .map(entity -> entity.toDomain(entity));
    }

}
