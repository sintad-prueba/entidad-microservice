package com.sintad.entidadmicroservice.infraestructure.adapter.entity;

import com.sintad.entidadmicroservice.domain.model.Entidad;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 *
 * @author roberth
 */
@Table(value = "tb_entidad")
@Getter
@Setter
@Builder
public class EntidadEntity {
    @Id
    private Long idEntidad;
    private Long idTipoDocumento;
    private String nroDocumento;
    private String razonSocial;
    private String nombreComercial;
    private Long idTipoContribuyente;
    private String direccion;
    private String telefono;
    private boolean estado;
    
    public Entidad toDomain(EntidadEntity entity) {
        return Entidad.builder()
                .idEntidad(entity.getIdEntidad())
                .idTipoDocumento(entity.getIdTipoDocumento())
                .nroDocumento(entity.getNroDocumento())
                .razonSocial(entity.getRazonSocial())
                .nombreComercial(entity.getNombreComercial())
                .idTipoContribuyente(entity.getIdTipoContribuyente())
                .direccion(entity.getDireccion())
                .telefono(entity.getTelefono())
                .build();
    }

    public EntidadEntity fromDomain(Entidad model) {
        return EntidadEntity.builder()
                .idEntidad(model.getIdEntidad())
                .idTipoDocumento(model.getIdTipoDocumento())
                .nroDocumento(model.getNroDocumento())
                .razonSocial(model.getRazonSocial())
                .nombreComercial(model.getNombreComercial())
                .idTipoContribuyente(model.getIdTipoContribuyente())
                .direccion(model.getDireccion())
                .telefono(model.getTelefono())
                .build();
    }
}
